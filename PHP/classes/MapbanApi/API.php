<?php
/**
 * User: Sascha Wendt
 * Date: 09.04.2019
 * Time: 10:29
 */

namespace mapban\api;

class API {
    
    protected $apiKey;
    
    const API_URL = "https://api.mapban.gg/v1/";
    
    public function __construct($apiKey) {
        $this->apiKey = $apiKey;
    }

    protected function sendGetRequest($endpoint) {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, true );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'accept: application/json',
            'Authorization: Bearer ' . $this->apiKey
        ) );
        
        $url  = self::API_URL . $endpoint;
        curl_setopt( $ch, CURLOPT_URL, $url );
        $result = curl_exec( $ch );

        if (!curl_errno( $ch )) {
            $info     = curl_getinfo( $ch );
            $response = json_decode( $result );
            if ($info['http_code'] != 200) {
                $message = null;
                if (isset( $response->error )) {
                    $message = "[" . $response->error->code . "]" .$response->error->description;
                }
                throw new \Exception( $message, $info['http_code'] );
            }
            return $response;
        }
        return false;
    }

    protected function sendDeleteRequest($endpoint, $data=null) {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, true );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, "DELETE" );
        if($data !== null) {
            $bodyData = json_encode($data);
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $bodyData );
        }
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'accept: application/json',
            'Authorization: Bearer ' . $this->apiKey
        ) );

        $url  = self::API_URL . $endpoint;
        curl_setopt( $ch, CURLOPT_URL, $url );
        $result = curl_exec( $ch );

        if (!curl_errno( $ch )) {
            $info     = curl_getinfo( $ch );
            $response = json_decode( $result );
            if ($info['http_code'] != 200) {
                $message = null;
                if (isset( $response->error )) {
                    $message = "[" . $response->error->code . "]" .$response->error->description;
                }
                throw new \Exception( $message, $info['http_code'] );
            }
            return $response;
        }
        return false;
    }
    
    protected function sendPostRequest($endpoint, $data) {
        $bodyData = json_encode($data);
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, true );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $bodyData );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'accept: application/json',
            'Authorization: Bearer ' . $this->apiKey,
            'Content-Length: '.strlen($bodyData)
        ) );
        
        $url  = self::API_URL . $endpoint;
        curl_setopt( $ch, CURLOPT_URL, $url );
        $result = curl_exec( $ch );

        if (!curl_errno( $ch )) {
            $info     = curl_getinfo( $ch );
            $response = json_decode( $result );
            if ($info['http_code'] != 200) {
                $message = null;
                if (isset( $response->error )) {
                    $message = "[" . $response->error->code . "]" .$response->error->description;
                }
                throw new \Exception( $message, $info['http_code'] );
            }
            return $response;
        }
        return false;
    }
}