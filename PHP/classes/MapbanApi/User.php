<?php
/**
 * User: Sascha Wendt
 * Date: 21.05.2019
 * Time: 10:03
 */

namespace mapban\api;


class User extends API {
    
    public $username;
    public $level;
    public $brandingFree;
	public $validUntil;
	public $lastLogin;

    public function __construct($apiKey, $data) {
        parent::__construct( $apiKey );
        $this->username = $data->username;
        $this->level = $data->level;
        $this->brandingFree = $data->brandingFree;
		$this->validUntil = $data->validUntil;
		$this->lastLogin = $data->lastLogin;
    }
    
    public static function getUser($apiKey) {
		$api = new API($apiKey);
		$userData = $api->sendGetRequest("user");
		return new User($apiKey, $userData);
    }

}