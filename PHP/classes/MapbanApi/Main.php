<?php
/**
 * User: Sascha Wendt
 * Date: 09.04.2019
 * Time: 10:25
 */

namespace mapban\api;


class Main extends API {

    public function __construct($apiKey) {
        parent::__construct( $apiKey );
    }

	public function getUser() {
        return \mapban\api\User::getUser($this->apiKey);
    }
	
    public function getGames() {
        return \mapban\api\Game::getGames($this->apiKey);
    }
    
    public function getGame($gameId) {
        return \mapban\api\Game::getGame($this->apiKey, $gameId);
    }
    
    public function getBanLobby($lobbyId) {
        return \mapban\api\BanLobby::getLobby($this->apiKey, $lobbyId);
    }
    
    public function createBanLobby($gameId, $mapIdArray, $bo, array $settings = null, array $voteOrder = null, array $teamNames = null) {
        return \mapban\api\BanLobby::createLobby($this->apiKey, $gameId, $mapIdArray, $bo, $settings, $voteOrder, $teamNames);
    }

    public function createBanLobbyData($gameId, $mapIdArray, $bo, array $settings = null, array $voteOrder = null, array $teamNames = null) {
        return \mapban\api\BanLobby::createLobbyData($gameId, $mapIdArray, $bo, $settings, $voteOrder, $teamNames);
    }

    public function createBanLobbies($data) {
        return \mapban\api\BanLobby::createLobbies($this->apiKey, $data);
    }

    public function deleteBanLobby($lobbyId) {
        return \mapban\api\BanLobby::deleteLobbyEx($this->apiKey, $lobbyId);
    }

}