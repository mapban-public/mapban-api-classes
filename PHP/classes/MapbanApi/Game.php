<?php
/**
 * User: Sascha Wendt
 * Date: 09.04.2019
 * Time: 10:25
 */

namespace mapban\api;


class Game extends API {
    
    public $id;
    public $name;
    public $image;

    public function __construct($apiKey, $data) {
        parent::__construct( $apiKey );
        $this->id = $data->id;
        $this->name = $data->name;
        $this->image = $data->image;
    }
    
    public function getAllMaps() {
        return $this->sendGetRequest("games/$this->id/maps")->maps;
    }
    
    public function getMap($mapId) {
        $mapId = strtolower($mapId);
        return $this->sendGetRequest("games/$this->id/maps/$mapId");
    }
    
    public function getAllMappools() {
        return $this->sendGetRequest("games/$this->id/mappools")->mappools;
    }
    
    public function getMappool($mappoolId) {
        $mappoolId = strtolower($mappoolId);
        return $this->sendGetRequest("games/$this->id/mappools/$mappoolId");
    }
    
    public function getDefaultVoteorder($bestof, $mapcount) {
        return $this->sendGetRequest("games/$this->id/voteorder/$bestof/$mapcount")->voteOrders;
    }

    public static function getGames($apiKey) {
        $api = new API($apiKey);
        $games = $api->sendGetRequest("games");
        $gamesArray = array();
        foreach($games->games as $game) {
            $gamesArray[] = new Game($apiKey, $game);
        }
        return($gamesArray);
    }
    
    public static function getGame($apiKey, $gameId) {
        $games = self::getGames($apiKey);
        foreach($games as $game) {
            if($game->id == strtolower($gameId)) {
                return new Game($apiKey, $game);
            }
        }
       return null;
    }

}