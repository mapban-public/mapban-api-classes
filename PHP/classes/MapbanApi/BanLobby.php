<?php
/**
 * User: Sascha Wendt
 * Date: 09.04.2019
 * Time: 10:25
 */

namespace mapban\api;


class BanLobby extends API {
    
    public $lobbyId;
    public $teamId;
    public $viewId;
    public $urls;
    public $teams;
    public $created;
    public $game;
    public $bo;
    public $mappoolKey;
    public $mapIds;
    public $voteOrder;
    public $settings;
    public $status;
    public $finishedMaps;
    public $log;
    public $sidePickData;

    public function __construct($apiKey, $data) {
        parent::__construct( $apiKey );
        $this->lobbyId = $data->lobbyId;
        $this->teamId = $data->teamId;
        $this->viewId = $data->viewId;
        $this->urls = $data->urls;
        $this->game = $data->game;
        $this->bo = $data->bo;
        $this->mappoolKey = $data->mappoolKey;
        $this->mapIds = $data->mapIds;
        $this->voteOrder = $data->voteOrder;
        $this->settings = $data->settings;
        if(isset($data->teams)) {
            $this->teams = $data->teams;
            $this->status = $data->status;
            $this->finishedMaps = $data->finishedMaps;
            $this->log = $data->log;
            $this->sidePickData = $data->sidePickData;
        }
    }
    
    public static function getLobby($apiKey, $lobbyId) {
        $api = new API($apiKey);
        $lobby = $api->sendGetRequest("ban/lobby/$lobbyId");
        return new BanLobby($apiKey, $lobby->lobby);
    }

    public  function delete() {
        return self::deleteLobbyEx($this->apiKey, $this->lobbyId);
    }

    public static function deleteLobbyEx($apiKey, $lobbyid) {
        $api = new API($apiKey);
        return $api->sendDeleteRequest("ban/lobby/$lobbyid");
    }
    
    public static function createLobby($apiKey, $gameId, array $mapIdArray, $bo, array $settings = null, array $voteOrder = null, array $teamNames = null) {
        $data = self::createLobbyData($gameId, $mapIdArray, $bo, $settings, $voteOrder, $teamNames);
        $api = new API($apiKey);
        $lobby = $api->sendPostRequest("ban/lobby", $data);
        return new BanLobby($apiKey, $lobby->lobby);
    }

    public static function createLobbyData($gameId, array $mapIdArray, $bo, array $settings = null, array $voteOrder = null, array $teamNames = null) {
        $data = new \stdClass();
        $data->game = $gameId;
        $data->mappool = $mapIdArray;
        $data->bo = $bo;
        if($settings !== null && is_array($settings)) {
            $data->settings = $settings;
        }
        if($voteOrder !== null && is_array($voteOrder)) {
            $data->voteOrder = $voteOrder;
        }
        if($teamNames !== null && is_array($teamNames)) {
            $data->teamNames = $teamNames;
        }
        return $data;
    }

    public static function createLobbies($apiKey, array $data) {
        $api = new API($apiKey);
        $lobbyData = $api->sendPostRequest("ban/lobby", $data);
        $lobbyArray = array();
        foreach ($lobbyData->lobbies as $lobby) {
            $lobbyArray[] = new BanLobby($apiKey, $lobby);
        }
        return $lobbyArray;
    }
	
	public function getSettings() {
		$response = $this->sendGetRequest("ban/lobby/$this->lobbyId/settings");
		$this->settings = $response->settings;
		return $response->settings;
	}
	
	public function setSettings(array $settings) {
		$data = new \stdClass();
        $data->settings = $settings;
		$response = $this->sendPostRequest("ban/lobby/$this->lobbyId/settings", $data);
		
		$this->settings = $response->settings;
		return $response->settings;
	}
	
	public function getVoteOrder() {
		$response = $this->sendGetRequest("ban/lobby/$this->lobbyId/voteorder");
		$this->voteOrder = $response->voteOrder;
		return $response->voteOrder;
	}
	
	public function setVoteOrder(array $voteOrder) {
		$data = new \stdClass();
        $data->voteOrder = $voteOrder;
		$response = $this->sendPostRequest("ban/lobby/$this->lobbyId/voteorder", $data);
		
		$this->voteOrder = $response->voteOrder;
		return $response->voteOrder;
	}
	
	public function getTeamNames() {
		$response = $this->sendGetRequest("ban/lobby/$this->lobbyId/teamnames");
		if(!is_array($this->teams)) {
			$this->teams = array();
			$this->teams[0] = new \stdClass();
			$this->teams[1] = new \stdClass();
		}
		$this->teams[0]->name = $response->teamNames[0];
		$this->teams[1]->name = $response->teamNames[1];
		return $response->teamNames;
	}
	
	public function setTeamNames($teamName1=null, $teamName2=null) {
		$data = new \stdClass();
        $data->teamNames = array($teamName1, $teamName2);
		$response = $this->sendPostRequest("ban/lobby/$this->lobbyId/teamnames", $data);
		
		if(!is_array($this->teams)) {
			$this->teams = array();
			$this->teams[0] = new \stdClass();
			$this->teams[1] = new \stdClass();
		}
		$this->teams[0]->name = $response->teamNames[0];
		$this->teams[1]->name = $response->teamNames[1];
		return $response->teamNames;
	}

}