<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
function classAutoloader($class) {
    if (preg_match( '/^mapban\\\\api\\\\([a-zA-Z]+)$/', $class, $matches )) {
        include_once("classes/MapbanApi/" . $matches[1] . ".php");
    }
}

spl_autoload_register( "classAutoloader" );

$apiKey = "YourApiKey";
$mapban = new \mapban\api\Main($apiKey);
echo "User:<pre>";
var_dump($mapban->getUser());
echo "</pre><br>";

echo "Games:<pre>";
var_dump($mapban->getGames());
echo "</pre><br>";

echo "Game:<pre>";
$r6s = $mapban->getGame("R6S");
var_dump($r6s);
echo "</pre><br>";

echo "Maps (R6S):<pre>";
var_dump($r6s->getAllMaps());
echo "</pre><br>";

echo "Border (R6S):<pre>";
var_dump($r6s->getMap("bOrDer"));
echo "</pre><br>";

echo "Mappools (R6S):<pre>";
var_dump($r6s->getAllMappools());
echo "</pre><br>";

echo "Mappool (EsL) (R6S):<pre>";
$r6sEsl = $r6s->getMappool("eSl");
var_dump($r6sEsl);
echo "</pre><br>";

echo "Voteorder Bo 3(EsL) (R6S):<pre>";
var_dump($r6s->getDefaultVoteorder(3, count($r6sEsl->mapIds)));
echo "</pre><br>";

echo "<br><hr><br>Lobby (llz5f1z8eG7MME6V):<pre>";
$lobby = $mapban->getBanLobby("llz5f1z8eG7MME6V");
var_dump($lobby);
echo "</pre><br>";

echo "<br><hr><br>Lobby get Settings:<pre>";
var_dump($lobby->getSettings());
echo "</pre><br>";

echo "<br><hr><br>Lobby set Settings:<pre>";
$settings= array();
$settings['sidePickDecider'] = false;
var_dump($lobby->setSettings($settings));
echo "</pre><br>";

echo "<br><hr><br>Lobby get Vote Order:<pre>";
var_dump($lobby->getVoteOrder());
echo "</pre><br>";

echo "<br><hr><br>Lobby set Vote Order:<pre>";
$voteOrder= array("Ban2", "Ban1", "Ban2", "Ban1", "Pick2", "Pick1", "Decider");
var_dump($lobby->setVoteOrder($voteOrder));
echo "</pre><br>";

echo "<br><hr><br>Lobby get Teamnames:<pre>";
var_dump($lobby->getTeamNames());
echo "</pre><br>";

echo "<br><hr><br>Lobby set Teamnames:<pre>";
var_dump($lobby->setTeamNames("Team1", "Team2"));
echo "</pre><br>";

echo "<br><hr><br>Lobby set Teamnames2:<pre>";
var_dump($lobby->setTeamNames(null, "Team2 is better"));
echo "</pre><br>";

echo "<br><hr><br>Lobby after changes:<pre>";
var_dump($lobby);
echo "</pre><br>";

echo "<br><hr><br><hr><br>CreateLobby:<pre>";
$teamnames = array("teamname 1","teamname 2");
//var_dump($mapban->createBanLobby($r6s->id, $r6sEsl->mapIds, 3, null, null, $teamnames));
echo "</pre><br>";

echo "<br><hr><br><hr><br>CreateLobbies:<pre>";
$data =array();
$data[] = $mapban->createBanLobbyData($r6s->id, $r6sEsl->mapIds, 3, null, null, $teamnames);
$data[] = $mapban->createBanLobbyData($r6s->id, $r6sEsl->mapIds, 3, null, null, array("team 1", "team 2"));
//var_dump($mapban->createBanLobbies($data));
echo "</pre><br>";

echo "<br><hr><br><hr><br>Delete Lobby:<pre>";
//var_dump($mapban->deleteBanLobby("msMMphaXlQL6YZ2C"));
//var_dump($mapban->deleteBanLobby("s5Gs3tPczNw77oDz"));
echo "</pre><br>";
?>